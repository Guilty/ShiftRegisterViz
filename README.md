# ShiftRegisterViz
A simple shift register design in SystemVerilog with an accompanying simulation
utilizing verilator, written in C++ which utilizes SDL to create a visualisation
of the design, and it's state at each simulation tick.  

This repository serves as a proof of concept for visualising a SystemVerilog design
during simulation.

## Building and Running
1. First Clone the repository:  

```
git clone https://codeberg.org/Guilty/ShiftRegisterViz
```

2. Install Dependencies  
Ubuntu:  
```
sudo apt install verilator make libsdl2-dev
```

Void Linux:  
```
xbps-install -S verilator make SDL2-devel
```
  
Should be the similar for other linux distros / package managers, sdl2 package names appear
to vary quite a bit, but simple search in your repos for `sdl2` should get you the package
you need.  

Also note that sdl doesn't work from within WSL, so neither will this project.
  
3. Running `make`  
```
cd ShiftRegisterViz/ && make
```
  
Which should run verilator, build an executable and run said executable.
To advance the simulation a single tick, press ENTER in `stdin`

