CPP_SRC_DIR := src

SDL_CFLAGS = `sdl2-config --cflags`
SDL_LDFLAGS = `sdl2-config --libs`
CFLAGS = -I$(CPP_INC_DIR)

ifeq ($(VERILATOR_ROOT),)
VERILATOR = verilator
else
export VERILATOR_ROOT
VERILATOR = $(VERILATOR_ROOT)/bin/verilator
endif

# TODO this target should probably be called all
default:
	@echo "--------------------------------------"
	@echo "Verilator / SDL Example Visualisation"
	@echo "--------------------------------------"
	@echo ""
	@echo "-> Building"
	$(VERILATOR) -Wall -cc ShiftRegister.sv ShiftRegister.cpp src/*.cpp --trace --build --exe -o ShiftRegister \
		-CFLAGS "-I../include ${SDL_CFLAGS}" -LDFLAGS "${SDL_LDFLAGS}"
	@echo "-> Running"
	obj_dir/ShiftRegister
	@echo "-> Done"

clean:
	@echo "-> Cleaning"
	rm -rf obj_dir *.log *.dmp *.vpd *.vcd
	@echo "-> Done"
