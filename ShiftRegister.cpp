#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <bitset>
#include <random>
#include <typeinfo>
#include <SDL.h>
#include "verilated.h"
#include "verilated_vcd_c.h" // required for trace generation
#include "VShiftRegister.h"
#include "SdlRegister.h"

#define WIDTH 3
#define H_RES 640
#define V_RES 480

typedef struct Pixel {
  uint8_t a; // transparency
  uint8_t b; // blue
  uint8_t g; // green
  uint8_t r; // red
} Pixel;

void tick(unsigned tickcount, VShiftRegister *dut, VerilatedVcdC* tfp) {
  dut->eval();
  if (tfp) tfp->dump((tickcount * 10) - 2);

  dut->clk = 1;

  dut->eval();
  if (tfp) tfp->dump(tickcount * 10);

  dut->clk = 0;

  dut->eval();
  if (tfp) {
    tfp->dump((tickcount * 10) + 5);
    tfp->flush();
  }
}

int setup_sdl(SDL_Window** window, SDL_Renderer** renderer) { 
  *window = SDL_CreateWindow("Square", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                                H_RES, V_RES, SDL_WINDOW_SHOWN);
  if (!window) {
    std::cout << "Window Creation failed %s" << SDL_GetError() << std::endl;
    return 1;
  }
  *renderer = SDL_CreateRenderer(*window, -1,
                                    SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
  if (!renderer) {
    std::cout << "Renderer Creation failed %s" << SDL_GetError() << std::endl;
    return 1;
  }
  return 0;
}

void annihilate_sdl(SDL_Window* window, SDL_Renderer* renderer) {
  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);
  SDL_Quit();
}

// SDL
// TODO: Clean up the sdl code into a seperate file
// TODO: Create a Rectangle to represent a register
// TODO: Draw WIDTH registers on the screen and chain them together
// TODO: Pause sim on each tick
// TODO: Display current values of each wire on the screen

// Testbench
// TODO: Add automated checking of results
// TODO: How to access SystemVerilog params from C++
// TODO: Seed the random number generator
// TODO: serialData should probably be longer than WIDTH
int main(int argc, char **argv) {
  //
  // SDL 
  //
  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
     std::cout << "SDL init failed." << std::endl;
     return 1;
  }
  SDL_Renderer* sdl_renderer = NULL;
  SDL_Window*   sdl_window   = NULL;

  setup_sdl(&sdl_window, &sdl_renderer);
  if(!sdl_window || !sdl_renderer) { return 1; }

  // Initialize Framerate
  uint64_t start_ticks = SDL_GetPerformanceCounter();
  uint64_t frame_count = 0;


  SDL_Rect rectangle_0 = {(H_RES/2)-(50/2), (V_RES/2)-(75/2), 50, 75};
  SDL_Rect rectangle_1 = {rectangle_0.x - rectangle_0.w - (20)*2, (V_RES/2)-(75/2), 50, 75};
  SDL_Rect rectangle_2 = {rectangle_0.x + rectangle_0.w + (20)*2, (V_RES/2)-(75/2), 50, 75};

  // centered rectangle
  std::vector<SdlRegister> sdlRegisters;
  sdlRegisters.push_back(SdlRegister(&sdl_renderer, &rectangle_0));
  sdlRegisters.push_back(SdlRegister(&sdl_renderer, &rectangle_1));
  sdlRegisters.push_back(SdlRegister(&sdl_renderer, &rectangle_2));
  std::for_each(sdlRegisters.begin(), sdlRegisters.end(), 
           [](SdlRegister sdlRegister){ sdlRegister.draw(); });

  // SdlRegister sdlRegister_0 = SdlRegister(&sdl_renderer, &rectangle_0);
  // SdlRegister sdlRegister_1 = SdlRegister(&sdl_renderer, &rectangle_1);
  // SdlRegister sdlRegister_2 = SdlRegister(&sdl_renderer, &rectangle_2);

  SDL_RenderPresent(sdl_renderer);

  //
  // Verilator
  //
  Verilated::commandArgs(argc, argv);
  Verilated::traceEverOn(true);

  // Instantiate DUT
  VShiftRegister *dut = new VShiftRegister;

  // Generate a trace
  VerilatedVcdC* tfp = new VerilatedVcdC;
  dut->trace(tfp, 99);
  tfp->open("ShiftRegisterTrace.vcd");
  
  //-------------------//
  // Generate Stimulus //
  //-------------------//
  std::mt19937 generator;
  std::bernoulli_distribution distribution;

  std::bitset<WIDTH> serialData;
  for (unsigned i=0; i<WIDTH; i++) {
    if(distribution(generator)) serialData.set(i);
    else serialData.reset(i);
  }
 

  //-----------------//
  // Simulation Loop //
  //-----------------//
  std::bitset<WIDTH> parallelData;
  std::cout << "input = " << serialData << std::endl;
  std::cout << "tick" << "\t|\t" << "output" << std::endl;
  for (unsigned i = 1; i <= WIDTH+1; i++) {
    dut->serialData = serialData[WIDTH-i];
    parallelData = std::bitset<WIDTH>(dut->parallelData);

    std::cout << "i = " << i << "\t|\t";
    std::cout << parallelData << std::endl;
   // for(int j = 0; j < parallelData.size(); j++) {
   //   if
   // }
   // SDL_SetRenderDrawColor(*(this->renderer), 0xFF, 0xFF, 0xFF, 0xFF);
    tick(i, dut, tfp);
    std::cin.get();
    SDL_RenderPresent(sdl_renderer);
  }
  dut->final();
  annihilate_sdl(sdl_window, sdl_renderer);
  return 0;
}
