#include "SdlRegister.h"
#include <SDL.h>

SdlRegister::SdlRegister(SDL_Renderer** renderer, SDL_Rect* rectangle) :
  renderer(renderer), rectangle(rectangle), wire_length(20), d(0), q(0) {}

// TODO: add triangle denoting clock and lines into and out of it to denote d & q
void SdlRegister::draw() {
  SDL_SetRenderDrawColor(*(this->renderer), 0xFF, 0xFF, 0xFF, 0xFF);
  SDL_RenderDrawRect(*(this->renderer), this->rectangle);

  this->drawWireD();
  // Draw Clk triangle (ugly as fuck lmao)
  int x1 = (this->rectangle)->x + (this->wire_length/2);
  int y1 = (this->rectangle)->y;
  int x2 = (this->rectangle)->x + ((this->rectangle)->w / 2);
  int y2 = (this->rectangle)->y + ((this->rectangle)->h / 4);
  SDL_RenderDrawLine(*(this->renderer), x1, y1, x2, y2);
  x1 += ((this->rectangle)->w - (this->wire_length));
  SDL_RenderDrawLine(*(this->renderer), x1, y1, x2, y2);
}

void SdlRegister::drawWireD() {
  // Draw Wires
  int x1 = (this->rectangle)->x; 
  int y1 = ((this->rectangle)->y) + (((this->rectangle)->h)/2);
  int x2 = x1 - this->wire_length;
  int y2 = y1;
  SDL_RenderDrawLine(*(this->renderer), x1, y1, x2, y2);
  this->drawWireQ(x1, x2, y1, y2);
}
void SdlRegister::drawWireQ(int x1, int x2, int y1, int y2) {
  x1 += ((this->rectangle)->w + this->wire_length);
  x2 = x1 - this->wire_length;
  SDL_RenderDrawLine(*(this->renderer), x1, y1, x2, y2);
}
