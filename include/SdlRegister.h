#ifndef SDLREGISTER_H
#define SDLREGISTER_H

#include <SDL.h>
#include <bitset>

// TODO make this able to handle more than just one bit wide symbols
// TODO it might be an idea to have a function which returns all the info
// an outside SDL Renderer might want 
class SdlRegister {
private: 
  SDL_Renderer** renderer; // So It can draw
  SDL_Rect* rectangle;

public:
  const int wire_length;
  std::bitset<1> d;
  std::bitset<1> q;
  SdlRegister(SDL_Renderer**, SDL_Rect*);
  SdlRegister(SDL_Renderer**, SDL_Rect*, int, int);
  void draw();
  void drawWire();
  void drawWireD();
  void drawWireQ(int, int, int, int);
};
#endif
