// TODO Add valid signal for parallelData output 
module ShiftRegister #( parameter integer N = 3 ) 
                      ( input  logic        clk,
                        input  logic        serialData,
                        output logic[N-1:0] parallelData );
logic[N-1:0] q;

always_ff @(posedge clk) q[0] <= serialData;
assign parallelData[0] = q[0];

genvar i;
generate
  for (i=1; i<N; i++) begin: RegisterChain
    always_ff @(posedge clk) q[i] <= q[i-1];
    assign parallelData[i] = q[i];
  end: RegisterChain
endgenerate

endmodule
